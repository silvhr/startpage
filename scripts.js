/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */

/**
 * Search function
 */

const searchInput = document.querySelector("#searchbar > input")
const searchButton = document.querySelector("#searchbar > button")

const lookup = {"/":"/","deepl":"https://deepl.com/","reddit":"https://reddit.com/","maps":"https://maps.google.com/"}
const engine = "duckduckgo"
const engineUrls = {
  deepl: "https://www.deepl.com/translator#-/-/",
  duckduckgo: "https://duckduckgo.com/?q=",
  ecosia: "https://www.ecosia.org/search?q=",
  google: "https://www.google.com/search?q=",
  startpage: "https://www.startpage.com/search?q=",
  youtube: "https://www.youtube.com/results?q=",
}

const isWebUrl = value => {
  try {
    const url = new URL(value)
    return url.protocol === "http:" || url.protocol === "https:"
  } catch {
    return false
  }
}

const getTargetUrl = value => {
  if (isWebUrl(value)) return value
  if (lookup[value]) return lookup[value]
  return engineUrls[engine] + value
}

const search = () => {
  const value = searchInput.value
  const targetUrl = getTargetUrl(value)
  window.open(targetUrl, "_self")
}

searchInput.onkeyup = event => event.key === "Enter" && search()
searchButton.onclick = search

/**
 * inject bookmarks into html
 */

const bookmarks = [{"id":"Vefja7kE9oAED8CO","label":"reddit","bookmarks":[{"id":"ajcYcJiiNWDn8V3V","label":"r/startpages","url":"https://www.reddit.com/r/startpages/"},{"id":"OODNVtUUmiGjq5ab","label":"r/typescript","url":"https://www.reddit.com/r/typescript/"},{"id":"vGJ0e4X8jpL0KeAa","label":"r/reactjs","url":"https://www.reddit.com/r/reactjs/"}]},{"id":"qPPkifXjrSK4ZwY2","label":"design tools","bookmarks":[{"id":"HaTU2MZdXAVvbsPJ","label":"pixlrx","url":"https://pixlr.com/x/"},{"id":"Mu5lEYQWRr7iDU0B","label":"image enlarger","url":"https://bigjpg.com/en"},{"id":"mbfUEqLc22fn9Qj3","label":"haikei","url":"https://app.haikei.app/"},{"id":"qbpQiyixCugNFSea","label":"css gradients","url":"https://larsenwork.com/easing-gradients/"}]},{"id":"e7d6XhzANV2feKuL","label":"worth reading","bookmarks":[{"id":"TBT3IbDK1oEsildg","label":"happy hues","url":"https://www.happyhues.co/"},{"id":"DccNHcDmQNOKmZJh","label":"styled-components","url":"https://www.joshwcomeau.com/react/demystifying-styled-components/"},{"id":"groWGvGURv4GgsTL","label":"react docs","url":"https://reactjs.org/docs/getting-started.html"}]},{"id":"sCocy9ShDp3PtLMj","label":"sources","bookmarks":[{"id":"QO6ze0jxOoyCe3JB","label":"icons","url":"https://feathericons.com/"},{"id":"CXTp06LOxNAS1E3G","label":"gif","url":"https://designyoutrust.com/2019/05/the-chill-and-retro-motion-pixel-art-of-motocross-saito/"},{"id":"AvnIzK9TC8AQOWo3","label":"@startpage","url":"https://prettycoffee.github.io/startpage"},{"id":"ruvJStcMCz7XGXua","label":"author","url":"https://prettycoffee.github.io/"}]}]

const createGroupContainer = () => {
  const container = document.createElement("div")
  container.className = "bookmark-group"
  return container
}

const createGroupTitle = title => {
  const h2 = document.createElement("h2")
  h2.innerHTML = title
  return h2
}

const createBookmark = ({ label, url }) => {
  const li = document.createElement("li")
  const a = document.createElement("a")
  a.href = url
  a.innerHTML = label
  li.append(a)
  return li
}

const createBookmarkList = bookmarks => {
  const ul = document.createElement("ul")
  bookmarks.map(createBookmark).forEach(li => ul.append(li))
  return ul
}

const createGroup = ({ label, bookmarks }) => {
  const container = createGroupContainer()
  const title = createGroupTitle(label)
  const bookmarkList = createBookmarkList(bookmarks)
  container.append(title)
  container.append(bookmarkList)
  return container
}

const injectBookmarks = () => {
  const bookmarksContainer = document.getElementById("bookmarks")
  bookmarksContainer.append()
  bookmarks.map(createGroup).forEach(group => bookmarksContainer.append(group))
}

injectBookmarks()
